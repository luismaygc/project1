<?php

namespace App\Http\Middleware;

use Closure;
use Psy\Exception\ErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $response = $next($request);
        /**
        *
        */
        if (isset($response->exception)) {
          if($response->exception instanceof NotFoundHttpException) {
            return redirect('/');
          }
        }
        /**
        *
        */
        return $response;
    }
}
