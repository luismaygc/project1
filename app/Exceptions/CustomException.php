<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception{

	public function report()
    {

    }
 
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        // return response()->view('auth.login');

        return redirect()->route('principal');
    }
}